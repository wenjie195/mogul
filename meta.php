<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta property="og:type" content="website" />
<meta property="og:image" content="https://mogul.capital/img/fb-meta.jpg" />
<meta name="author" content="Mogul Capital">
<meta property="og:description" content="Mogul Capital - Your Fastest Growing Wealth Management Partner. Malaysia-based Mogul Capital is a locally established and SSM registered Capital company with a unique blend of Wealth Management offering opportunities aimed at the global market stemming from Malaysia." />
<meta name="description" content="Mogul Capital - Your Fastest Growing Wealth Management Partner. Malaysia-based Mogul Capital is a locally established and SSM registered Capital company with a unique blend of Wealth Management offering opportunities aimed at the global market stemming from Malaysia." />
<meta name="keywords" content="mogul, mogul capital, wealth growing, Fastest Growing Wealth Management Partner, Malaysia, investor, stock, pre-ipo, ALIAC Main-Fund, asset, ai, marketing, etc">
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-5WW956WK7B"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-5WW956WK7B');
</script>
<?php
  $tz = 'Asia/Kuala_Lumpur';
  $timestamp = time();
  $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
  $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
  $time = $dt->format('Y');
?>