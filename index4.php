<?php
$connect = mysqli_connect("localhost", "root", "", "vidatech_mogul");
// $connect = mysqli_connect("localhost", "qlianmen_mguser", "Q7MkB9t8HSpi", "qlianmen_mogul");
// $connect = mysqli_connect("localhost", "mogulcap_mguser", "3dA523X7WmHe", "mogulcap_live");
// $connect = mysqli_connect("localhost", "hygenieg_mguser", "gVAcQok9FTh6", "hygenieg_mogul");

$query = '
SELECT sensors_temperature_data, 
UNIX_TIMESTAMP(CONCAT_WS(" ", sensors_data_date, sensors_data_time)) AS datetime 
FROM tbl_sensors_data 
ORDER BY sensors_data_date DESC, sensors_data_time DESC
';

$result = mysqli_query($connect, $query);
$rows = array();
$table = array();

$table['cols'] = array(
 array(
  'label' => 'Date Time', 
  'type' => 'datetime'
 ),
 array(
  'label' => 'Amount', 
  'type' => 'number'
 )
);

while($row = mysqli_fetch_array($result))
{
    $sub_array = array();
    $datetime = explode(".", $row["datetime"]);
    $sub_array[] =  array(
    "v" => 'Date(' . $datetime[0] . '000)'
    );

    $sub_array[] =  array(
    "v" => $row["sensors_temperature_data"]
    );

    $rows[] =  array(
    "c" => $sub_array
    );
}
$table['rows'] = $rows;
$jsonTable = json_encode($table);

?>

<html>
    <head>
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script type="text/javascript">
            google.charts.load('current', {'packages':['corechart']});
            google.charts.setOnLoadCallback(drawChart);
            function drawChart()
            {
				var container = document.getElementById('line_chart');
				var node = container.appendChild(document.createElement('div'));
				var chart = new google.visualization.ScatterChart(node);
				chart.draw(data, options);
            var data = new google.visualization.DataTable(<?php echo $jsonTable; ?>);
            var options = 
            {
                title:'BTC/BUSD',
				legendTextStyle: { color: '#76808F' },
    			titleTextStyle: { color: '#76808F' },
				colors: ['#00cccc'],
                legend:{position:'top'},
                chartArea:{width:'calc(100% - 120px)', height:'300px'},
				 backgroundColor: {
									fill:'black'     
									},
					hAxis: {
						textStyle:{color: '#76808F'}
					},
					vAxis: {
						textStyle:{color: '#76808F'}
					},					
            };
            var chart = new google.visualization.LineChart(document.getElementById('line_chart'));
            chart.draw(data, options);
            }
        </script>
        <style>
        .page-wrapper
        {
        width:100%;
        margin:0 auto;
        }
		rect{
			white-space:pre-wrap !important;}
        </style>
    </head>  
    <body>
        <div class="page-wrapper">
            <!-- <h2 align="center">Bitcoin USD</h2> -->
            <div id="line_chart" style="width: 100%; height: 300px"></div>
        </div>
    </body>
</html>