-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 05, 2021 at 10:09 AM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_mogul`
--

-- --------------------------------------------------------

--
-- Table structure for table `rates`
--

CREATE TABLE `rates` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rates`
--

INSERT INTO `rates` (`id`, `uid`, `name`, `amount`, `status`, `date_created`, `date_updated`) VALUES
(1, 'ed838de2fa58881de1b260937e6e2547', 'Bitcoin USD', '46762.35', NULL, '2021-03-05 08:59:28', '2021-03-05 08:59:28'),
(2, '6f840f57534216442683882a43a4e0af', 'Bitcoin USD', '46762.35', NULL, '2021-03-05 08:59:49', '2021-03-05 08:59:49'),
(3, '738086adb592cd278012bcb3fd5ea83c', 'Bitcoin USD', '46762.35', NULL, '2021-03-05 09:00:11', '2021-03-05 09:00:11'),
(4, 'f7cfb00e6ffe8e9b5bc0c7436505f0f6', 'Bitcoin USD', '46732.56', NULL, '2021-03-05 09:00:32', '2021-03-05 09:00:32'),
(5, '8edc1f0035c28be3bae76a2823e6cf33', 'Bitcoin USD', '46732.56', NULL, '2021-03-05 09:00:53', '2021-03-05 09:00:53'),
(6, 'df2d935b13d69715f3e7ac14b3f622f6', 'Bitcoin USD', '46732.56', NULL, '2021-03-05 09:01:14', '2021-03-05 09:01:14'),
(7, 'fc87029a1a6e472549d7fa405a4910fe', 'Bitcoin USD', '46755.41', NULL, '2021-03-05 09:01:35', '2021-03-05 09:01:35'),
(8, '57a8565699db736c0c73a3d1d3b350e3', 'Bitcoin USD', '46755.41', NULL, '2021-03-05 09:01:56', '2021-03-05 09:01:56'),
(9, '9de7c6be526b863c2bddb2a02f0f3b8b', 'Bitcoin USD', '46755.41', NULL, '2021-03-05 09:02:18', '2021-03-05 09:02:18'),
(10, '0b5ca495de3eb39391f048b57c39b253', 'Bitcoin USD', '46741.35', NULL, '2021-03-05 09:02:39', '2021-03-05 09:02:39'),
(11, '9bb9fb29539cb984fa88804c5a6122bd', 'Bitcoin USD', '46741.35', NULL, '2021-03-05 09:03:00', '2021-03-05 09:03:00'),
(12, 'bf6a46203314885868352ee2694ca615', 'Bitcoin USD', '46741.35', NULL, '2021-03-05 09:03:21', '2021-03-05 09:03:21'),
(13, '96794848dcccbfca5b621f88da9ea76b', 'Bitcoin USD', '46712.72', NULL, '2021-03-05 09:03:42', '2021-03-05 09:03:42'),
(14, 'a75d7748bef4ef4354ba46680ffa61cd', 'Bitcoin USD', '46712.72', NULL, '2021-03-05 09:04:03', '2021-03-05 09:04:03'),
(15, 'bd94b687955e41622017bee3e48cb365', 'Bitcoin USD', '46728.297', NULL, '2021-03-05 09:04:25', '2021-03-05 09:04:25'),
(16, 'c2e684f2ee6732705a012572f3b8e449', 'Bitcoin USD', '46728.297', NULL, '2021-03-05 09:04:46', '2021-03-05 09:04:46'),
(17, '3a4c9c6f0586645fcf7c93bbcb77c9e7', 'Bitcoin USD', '46728.297', NULL, '2021-03-05 09:05:07', '2021-03-05 09:05:07'),
(18, 'd25c5e7a28927080eac81fbb351315f0', 'Bitcoin USD', '46745.938', NULL, '2021-03-05 09:05:28', '2021-03-05 09:05:28'),
(19, '861b2375cacc907d66a96d2473c10332', 'Bitcoin USD', '46745.938', NULL, '2021-03-05 09:05:59', '2021-03-05 09:05:59'),
(20, 'f4288179e183d593c48b0fc4c063570f', 'Bitcoin USD', '46745.938', NULL, '2021-03-05 09:06:30', '2021-03-05 09:06:30'),
(21, 'a3c267553979299af9766f2ad23a2f87', 'Bitcoin USD', '46842.887', NULL, '2021-03-05 09:07:01', '2021-03-05 09:07:01'),
(22, '73124d5c33970fbd07805cadcbf85f49', 'Bitcoin USD', '46853.266', NULL, '2021-03-05 09:07:33', '2021-03-05 09:07:33'),
(23, '1293f2ae46294fab0a4776b90e389dee', 'Bitcoin USD', '46853.266', NULL, '2021-03-05 09:08:04', '2021-03-05 09:08:04'),
(24, 'f3b050cd2122a59df62e82f026f0043e', 'Bitcoin USD', '46867.56', NULL, '2021-03-05 09:08:35', '2021-03-05 09:08:35'),
(25, '2b2b5f7004188011a5b71da64a417c3c', 'Bitcoin USD', '46867.56', NULL, '2021-03-05 09:09:06', '2021-03-05 09:09:06');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `rates`
--
ALTER TABLE `rates`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `rates`
--
ALTER TABLE `rates`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
