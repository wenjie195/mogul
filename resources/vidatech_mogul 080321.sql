-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 08, 2021 at 09:08 AM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_mogul`
--

-- --------------------------------------------------------

--
-- Table structure for table `rates`
--

CREATE TABLE `rates` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rates`
--

INSERT INTO `rates` (`id`, `uid`, `name`, `amount`, `status`, `date_created`, `date_updated`) VALUES
(1, 'ed838de2fa58881de1b260937e6e2547', 'Bitcoin USD', '46762.35', NULL, '2021-03-05 08:59:28', '2021-03-05 08:59:28'),
(2, '6f840f57534216442683882a43a4e0af', 'Bitcoin USD', '46762.35', NULL, '2021-03-05 08:59:49', '2021-03-05 08:59:49'),
(3, '738086adb592cd278012bcb3fd5ea83c', 'Bitcoin USD', '46762.35', NULL, '2021-03-05 09:00:11', '2021-03-05 09:00:11'),
(4, 'f7cfb00e6ffe8e9b5bc0c7436505f0f6', 'Bitcoin USD', '46732.56', NULL, '2021-03-05 09:00:32', '2021-03-05 09:00:32'),
(5, '8edc1f0035c28be3bae76a2823e6cf33', 'Bitcoin USD', '46732.56', NULL, '2021-03-05 09:00:53', '2021-03-05 09:00:53'),
(6, 'df2d935b13d69715f3e7ac14b3f622f6', 'Bitcoin USD', '46732.56', NULL, '2021-03-05 09:01:14', '2021-03-05 09:01:14'),
(7, 'fc87029a1a6e472549d7fa405a4910fe', 'Bitcoin USD', '46755.41', NULL, '2021-03-05 09:01:35', '2021-03-05 09:01:35'),
(8, '57a8565699db736c0c73a3d1d3b350e3', 'Bitcoin USD', '46755.41', NULL, '2021-03-05 09:01:56', '2021-03-05 09:01:56'),
(9, '9de7c6be526b863c2bddb2a02f0f3b8b', 'Bitcoin USD', '46755.41', NULL, '2021-03-05 09:02:18', '2021-03-05 09:02:18'),
(10, '0b5ca495de3eb39391f048b57c39b253', 'Bitcoin USD', '46741.35', NULL, '2021-03-05 09:02:39', '2021-03-05 09:02:39'),
(11, '9bb9fb29539cb984fa88804c5a6122bd', 'Bitcoin USD', '46741.35', NULL, '2021-03-05 09:03:00', '2021-03-05 09:03:00'),
(12, 'bf6a46203314885868352ee2694ca615', 'Bitcoin USD', '46741.35', NULL, '2021-03-05 09:03:21', '2021-03-05 09:03:21'),
(13, '96794848dcccbfca5b621f88da9ea76b', 'Bitcoin USD', '46712.72', NULL, '2021-03-05 09:03:42', '2021-03-05 09:03:42'),
(14, 'a75d7748bef4ef4354ba46680ffa61cd', 'Bitcoin USD', '46712.72', NULL, '2021-03-05 09:04:03', '2021-03-05 09:04:03'),
(15, 'bd94b687955e41622017bee3e48cb365', 'Bitcoin USD', '46728.297', NULL, '2021-03-05 09:04:25', '2021-03-05 09:04:25'),
(16, 'c2e684f2ee6732705a012572f3b8e449', 'Bitcoin USD', '46728.297', NULL, '2021-03-05 09:04:46', '2021-03-05 09:04:46'),
(17, '3a4c9c6f0586645fcf7c93bbcb77c9e7', 'Bitcoin USD', '46728.297', NULL, '2021-03-05 09:05:07', '2021-03-05 09:05:07'),
(18, 'd25c5e7a28927080eac81fbb351315f0', 'Bitcoin USD', '46745.938', NULL, '2021-03-05 09:05:28', '2021-03-05 09:05:28'),
(19, '861b2375cacc907d66a96d2473c10332', 'Bitcoin USD', '46745.938', NULL, '2021-03-05 09:05:59', '2021-03-05 09:05:59'),
(20, 'f4288179e183d593c48b0fc4c063570f', 'Bitcoin USD', '46745.938', NULL, '2021-03-05 09:06:30', '2021-03-05 09:06:30'),
(21, 'a3c267553979299af9766f2ad23a2f87', 'Bitcoin USD', '46842.887', NULL, '2021-03-05 09:07:01', '2021-03-05 09:07:01'),
(22, '73124d5c33970fbd07805cadcbf85f49', 'Bitcoin USD', '46853.266', NULL, '2021-03-05 09:07:33', '2021-03-05 09:07:33'),
(23, '1293f2ae46294fab0a4776b90e389dee', 'Bitcoin USD', '46853.266', NULL, '2021-03-05 09:08:04', '2021-03-05 09:08:04'),
(24, 'f3b050cd2122a59df62e82f026f0043e', 'Bitcoin USD', '46867.56', NULL, '2021-03-05 09:08:35', '2021-03-05 09:08:35'),
(25, '2b2b5f7004188011a5b71da64a417c3c', 'Bitcoin USD', '46867.56', NULL, '2021-03-05 09:09:06', '2021-03-05 09:09:06'),
(26, 'fb33fc149bb0d31f7ca0e625acbc3495', 'Bitcoin USD', '46934.227', NULL, '2021-03-05 09:09:37', '2021-03-05 09:09:37'),
(27, '53b1b754fddf1cba52e1fa8d0aa52798', 'Bitcoin USD', '46934.227', NULL, '2021-03-05 09:10:09', '2021-03-05 09:10:09'),
(28, 'bc74cddb221c73995005677f1d52b7bc', 'Bitcoin USD', '46935.957', NULL, '2021-03-05 09:10:41', '2021-03-05 09:10:41'),
(29, 'e47036e2fdd35c6c31d7ebe8aa8184fb', 'Bitcoin USD', '46935.957', NULL, '2021-03-05 09:11:13', '2021-03-05 09:11:13'),
(30, '5dfd61bc4db747103e54459932a6b894', 'Bitcoin USD', '46913.504', NULL, '2021-03-05 09:11:45', '2021-03-05 09:11:45'),
(31, 'cff1d08b11128685bb03c8bb3e19171a', 'Bitcoin USD', '46913.504', NULL, '2021-03-05 09:12:17', '2021-03-05 09:12:17'),
(32, '4ba76089b355010ed96ca78cd5c58d17', 'Bitcoin USD', '46913.504', NULL, '2021-03-05 09:12:49', '2021-03-05 09:12:49'),
(33, '64f2abdd753e9f6ae553704eef104117', 'Bitcoin USD', '46913.504', NULL, '2021-03-05 09:13:21', '2021-03-05 09:13:21'),
(34, '1560071f9d23008b035be7501a0ec427', 'Bitcoin USD', '46867.06', NULL, '2021-03-05 09:13:53', '2021-03-05 09:13:53'),
(35, '9a4282b4678ba92c9e87f8642159c49a', 'Bitcoin USD', '46861.51', NULL, '2021-03-05 09:14:25', '2021-03-05 09:14:25'),
(36, 'b429a49368b4628771973b1068dda96e', 'Bitcoin USD', '46861.51', NULL, '2021-03-05 09:14:57', '2021-03-05 09:14:57'),
(37, '644794c22eff715f45ed2c53fea3c96b', 'Bitcoin USD', '46861.51', NULL, '2021-03-05 09:15:29', '2021-03-05 09:15:29'),
(38, '1093d086a40c587853f35f06df48cf3f', 'Bitcoin USD', '46888.348', NULL, '2021-03-05 09:16:01', '2021-03-05 09:16:01'),
(39, '7efeef6e49b3f252ebdf95d12f80090b', 'Bitcoin USD', '46880.348', NULL, '2021-03-05 09:16:33', '2021-03-05 09:16:33'),
(40, 'a9bd2d58ba499a1e4f2de6560cc3b26d', 'Bitcoin USD', '46880.348', NULL, '2021-03-05 09:17:05', '2021-03-05 09:17:05'),
(41, '495aefcb53b17969f47322480eb61fc8', 'Bitcoin USD', '46871.68', NULL, '2021-03-05 09:17:37', '2021-03-05 09:17:37'),
(42, '2cf9992bd199e908661748620a6bb05c', 'Bitcoin USD', '46871.68', NULL, '2021-03-05 09:18:09', '2021-03-05 09:18:09'),
(43, 'e5adb54a8db81ea528499e35b5746cbb', 'Bitcoin USD', '46813.066', NULL, '2021-03-05 09:18:41', '2021-03-05 09:18:41'),
(44, '94229b890fb12ab2cfa103d57298483f', 'Bitcoin USD', '46813.066', NULL, '2021-03-05 09:19:13', '2021-03-05 09:19:13'),
(45, 'f0894daeb3b664f4d7e3fef5ff98abbe', 'Bitcoin USD', '46820.133', NULL, '2021-03-05 09:19:45', '2021-03-05 09:19:45'),
(46, '91c18e61be1da4e9d0c5506eaa89a5c4', 'Bitcoin USD', '46820.133', NULL, '2021-03-05 09:20:17', '2021-03-05 09:20:17'),
(47, '661d91379b057c9777ca608f3b64afef', 'Bitcoin USD', '46827.97', NULL, '2021-03-05 09:20:49', '2021-03-05 09:20:49'),
(48, '67139aae28cf5b1c1abcfcebf5b784c1', 'Bitcoin USD', '46827.97', NULL, '2021-03-05 09:21:21', '2021-03-05 09:21:21'),
(49, '81e993be72d485ea994ba381c58398a9', 'Bitcoin USD', '46901.652', NULL, '2021-03-05 09:21:53', '2021-03-05 09:21:53'),
(50, 'e0a526d0ea222eb0fd33308485966ae4', 'Bitcoin USD', '46901.652', NULL, '2021-03-05 09:22:25', '2021-03-05 09:22:25'),
(51, '50ed3007a8f9ff66c8fdb5e75951314b', 'Bitcoin USD', '46887.62', NULL, '2021-03-05 09:22:57', '2021-03-05 09:22:57'),
(52, '44b7e688707bec2fd43de1f7f8be7310', 'Bitcoin USD', '46887.62', NULL, '2021-03-05 09:23:29', '2021-03-05 09:23:29'),
(53, 'daac357a1941e5b6811709acfa69db54', 'Bitcoin USD', '46889.36', NULL, '2021-03-05 09:24:01', '2021-03-05 09:24:01'),
(54, '0ef605f89bf1193bba227b1c2008d935', 'Bitcoin USD', '46886.28', NULL, '2021-03-05 09:24:33', '2021-03-05 09:24:33'),
(55, '4932b3555b329f1de692c5899e3c46fc', 'Bitcoin USD', '46886.28', NULL, '2021-03-05 09:25:04', '2021-03-05 09:25:04'),
(56, '4ed86c03746263e896116ea2f0e39771', 'Bitcoin USD', '46934.35', NULL, '2021-03-05 09:25:35', '2021-03-05 09:25:35'),
(57, 'dee1e1aa655f1d535ad7b452b48dc8a1', 'Bitcoin USD', '46934.35', NULL, '2021-03-05 09:26:06', '2021-03-05 09:26:06'),
(58, 'ba385a48d441c4f891da8a6aa338cc2b', 'Bitcoin USD', '46906.633', NULL, '2021-03-05 09:26:37', '2021-03-05 09:26:37'),
(59, '2e095ca0e826572d71250f1b90988408', 'Bitcoin USD', '46906.633', NULL, '2021-03-05 09:27:09', '2021-03-05 09:27:09'),
(60, '541170a4d9b6959ae30b5868b9087a77', 'Bitcoin USD', '47013.016', NULL, '2021-03-05 09:27:40', '2021-03-05 09:27:40'),
(61, 'a1ee36441f37bfa37b75d137e09f6557', 'Bitcoin USD', '47013.016', NULL, '2021-03-05 09:28:11', '2021-03-05 09:28:11'),
(62, '5351410b60adc86b9ac2641cfdae31d3', 'Bitcoin USD', '46996.656', NULL, '2021-03-05 09:28:42', '2021-03-05 09:28:42'),
(63, '60cf4cd4c022336ffa50be41674d889a', 'Bitcoin USD', '46996.656', NULL, '2021-03-05 09:29:13', '2021-03-05 09:29:13'),
(64, '33bf128250deff969181d2800337a570', 'Bitcoin USD', '46992.812', NULL, '2021-03-05 09:29:44', '2021-03-05 09:29:44'),
(65, 'c8b1d42d201cc9001cb48a80813e4c75', 'Bitcoin USD', '46992.812', NULL, '2021-03-05 09:30:15', '2021-03-05 09:30:15'),
(66, '54405a58d2d7e0d274afc972dbef28bd', 'Bitcoin USD', '46943.152', NULL, '2021-03-05 09:30:47', '2021-03-05 09:30:47'),
(67, '010055a78ebc953b4e4536d16ca912e9', 'Bitcoin USD', '46943.152', NULL, '2021-03-05 09:31:18', '2021-03-05 09:31:18'),
(68, '5be057d4e80324fda06e3415397bc93f', 'Bitcoin USD', '46934.41', NULL, '2021-03-05 09:31:49', '2021-03-05 09:31:49'),
(69, 'e4ca8ee47ab6bf72a28840c8e72c4978', 'Bitcoin USD', '46934.41', NULL, '2021-03-05 09:32:20', '2021-03-05 09:32:20'),
(70, '39e3ddf28809692e123f1cb59581920c', 'Bitcoin USD', '46939.844', NULL, '2021-03-05 09:32:51', '2021-03-05 09:32:51'),
(71, 'd9b4b0d5fb4680cad26fe44ca064f806', 'Bitcoin USD', '46939.844', NULL, '2021-03-05 09:33:22', '2021-03-05 09:33:22'),
(72, '96ca9b79779582e5912cef1f6c7ec799', 'Bitcoin USD', '46919.363', NULL, '2021-03-05 09:33:53', '2021-03-05 09:33:53'),
(73, 'b645a43b071ffd9a4279b3ebf08471a6', 'Bitcoin USD', '46919.363', NULL, '2021-03-05 09:34:24', '2021-03-05 09:34:24');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sensors_data`
--

CREATE TABLE `tbl_sensors_data` (
  `sensors_data_id` int(11) NOT NULL,
  `sensors_temperature_data` varchar(255) DEFAULT NULL,
  `sensors_data_date` varchar(255) DEFAULT NULL,
  `sensors_data_time` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_sensors_data`
--

INSERT INTO `tbl_sensors_data` (`sensors_data_id`, `sensors_temperature_data`, `sensors_data_date`, `sensors_data_time`) VALUES
(1, '49855.22', '2021-03-08', '16:06:08'),
(2, '49830.54', '2021-03-08', '16:06:30'),
(3, '49830.54', '2021-03-08', '16:06:52'),
(4, '49830.54', '2021-03-08', '16:07:14'),
(5, '49811.3', '2021-03-08', '16:07:36'),
(6, '49811.3', '2021-03-08', '16:07:57');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `rates`
--
ALTER TABLE `rates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_sensors_data`
--
ALTER TABLE `tbl_sensors_data`
  ADD PRIMARY KEY (`sensors_data_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `rates`
--
ALTER TABLE `rates`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `tbl_sensors_data`
--
ALTER TABLE `tbl_sensors_data`
  MODIFY `sensors_data_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
