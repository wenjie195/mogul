<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Rates.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$tz = 'Asia/Kuala_Lumpur';
$timestamp = time();
$dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
$dt->setTimestamp($timestamp); //adjust the object to correct timestamp
$currentDate = $dt->format('Y-m-d');
// echo "<br>";
$currentTime = $dt->format('H:i:s');

function addData($conn,$stockPrice,$currentDate,$currentTime)
{
     if(insertDynamicData($conn,"tbl_sensors_data",array("sensors_temperature_data","sensors_data_date","sensors_data_time"),
          array($stockPrice,$currentDate,$currentTime),"sss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

$conn = connDB();
$No = 0;
// create & initialize a curl session
$curl = curl_init();
// set our url with curl_setopt()
curl_setopt($curl, CURLOPT_URL, "https://financialmodelingprep.com/api/v3/quote/BTCUSD?apikey=24a4532838184ee30da9347b5aa38357");
// return the transfer as a string, also with setopt()
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
// curl_exec() executes the started curl session
// $output contains the output string
$output = curl_exec($curl);
// close curl resource to free up system resources
// (deletes the variable made by curl_init)
// curl_close($curl);
$exchangeData = json_decode($output, true);

    if ($exchangeData)
    {
        for ($i=0; $i <count($exchangeData) ; $i++)
        {
            if (strpos($exchangeData[$i]['name'],"TBD") == false )
            {
                $No++;
                $stockPrice = $exchangeData[$i]['price'];
            }
        }
    }

    if(addData($conn,$stockPrice,$currentDate,$currentTime))
    {
        echo "success";
        // header('Location: ../autoGetData.php');     
    }
    else
    {
        echo "fail";
    }

?>