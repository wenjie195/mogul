<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Rates.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$productDetails = getRates($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://mogul.capital/" />
<link rel="canonical" href="https://mogul.capital/" />
<meta property="og:title" content="Mogul Capital | Your Fastest Growing Wealth Management Partner" />
<title>Mogul Capital | Your Fastest Growing Wealth Management Partner</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<div class="width100 same-padding banner1 text-center">
	
    <p class="ow-first-p black-text first-p white-text wow fadeIn" data-wow-delay="0.3s">Your Fastest Growing Wealth Management Partner</p>
    <h1 class="darkgold-text first-h1 white-text wow fadeIn" data-wow-delay="0.6s">Mogul Capital</h1>
</div>
<div class="width100 same-padding banner2 overflow">
		<p class="black-text title-p wow fadeIn text-center ow-title-p" data-wow-delay="1s"><b>About</b> Us</p>
        <div class="short-gold-border text-center wow fadeIn" data-wow-delay="1.3s"></div>
    <div class="banner2-left-content">
    	<h1 class="darkgold-text banner2-h1 wow fadeIn" data-wow-delay="1.8s">Unique Blend of<br><b>Wealth Management</b></h1>
    </div>
	<div class="banner2-right-content">
    	
        <p class="content-p wow fadeIn ow-content-p black-text" data-wow-delay="2.1s">
        	Malaysia-based Mogul Capital is a locally established and SSM registered Capital company with a unique blend of Wealth Management offering opportunities aimed at the global market stemming from Malaysia.<br><br>

Built within with a team of <b>experienced entrepreneurial personnel, aggressive and disciplined investors</b>, Mogul Capital is well-equipped to spot and execute precisely esteemed <b>investment opportunities.</b><br><br>

The competent fund managers are self-made millionaires from various walks of life, ranging from a 20-year traditional business trader, investor and real estate investor to a serial entrepreneur and serial commercial project developer for 8 years.
        
        </p>
    </div>

</div>

<div class="width100 same-padding dark-bg overflow">
    <!-- <div class="right-dark-div"> -->
    <!-- <p class="btc-p white-text wow fadeIn"  data-wow-delay="2.3s"><b>BTC/BUSD</b></p> -->
    <!-- <div id="chart" class="wow fadeIn"  data-wow-delay="2.6s"></div> -->
    <div id="chartBTC" class="wow fadeIn"></div>
    <!-- </div> -->
</div>

<div class="width100 gmap-div"></div>

<?php include 'js.php'; ?>

<!-- <script type="text/javascript">
    $(document).ready(function()
    {
        $("#chartBTC").load("index4.php");
    setInterval(function()
    {
        $("#chartBTC").load("index4.php");
    }, 10000);
    });
</script> -->

<!-- Server User Cron Job will collet data every 1 min -->
<script type="text/javascript">
    $(document).ready(function()
    {
        $("#chartBTC").load("index4.php");
    setInterval(function()
    {
        $("#chartBTC").load("index4.php");
    }, 10000);
    });
</script>

</body>
</html>