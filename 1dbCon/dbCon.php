<?php
// DB SQL Connection
function connDB(){
    // Create connection
    $conn = new mysqli("localhost", "root", "","vidatech_mogul");//for localhost
    // $conn = new mysqli("localhost", "qlianmen_mguser", "Q7MkB9t8HSpi","qlianmen_mogul"); //for qlianmeng server
    // $conn = new mysqli("localhost", "mogulcap_mguser", "3dA523X7WmHe","mogulcap_live"); //for mogul server
    // $conn = new mysqli("localhost", "hygenieg_mguser", "gVAcQok9FTh6","hygenieg_mogul"); //for mogul server

    mysqli_set_charset($conn,'UTF8');//because cant show chinese characters so need to include this to show
    //for when u insert chinese characters inside, because if dont include this then it will become unknown characters in the database
    mysqli_query($conn,"SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'")or die(mysqli_error($conn));

// Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    return $conn;
}
