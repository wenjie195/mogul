<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Rates.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

// $uid = $_SESSION['uid'];

$conn = connDB();

$No = 0;
// create & initialize a curl session
$curl = curl_init();

// set our url with curl_setopt()
curl_setopt($curl, CURLOPT_URL, "https://financialmodelingprep.com/api/v3/quote/BTCUSD?apikey=24a4532838184ee30da9347b5aa38357");

// return the transfer as a string, also with setopt()
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

// curl_exec() executes the started curl session
// $output contains the output string
$output = curl_exec($curl);

// close curl resource to free up system resources
// (deletes the variable made by curl_init)
// curl_close($curl);

$exchangeData = json_decode($output, true);
?>

<?php
if ($exchangeData)
{
    for ($i=0; $i <count($exchangeData) ; $i++)
    {
        // if (strpos($exchangeData[$i]['name'],"TBD") == false && date('Y-m-d',strtotime($exchangeData[$i]['begin_at'])) > date('Y-m-d'))
        if (strpos($exchangeData[$i]['name'],"TBD") == false )
        {
        $No++;
        ?>
        
            <form name="xyz" action="utilities/autoGetDataFunction.php" method="post">
            <!-- <form action="utilities/autoGetDataFunction.php" method="post"> -->
                <input type="text" name="stock_name" value="<?php echo $exchangeData[$i]['name'] ?>">
                <input type="text" name="stock_price" value="<?php echo $exchangeData[$i]['price'] ?>">
            </form>

        <?php
        }
    }
}
?>

<!-- <script type="text/javascript">
    var wait=setTimeout("document.xyz.submit();",30000);
</script> -->

<!-- Server User Cron Job , NO NEED THIS FUNCTION -->
<script type="text/javascript">
    var wait=setTimeout("document.xyz.submit();",1000);
</script>