<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Rates.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$productDetails = getRates($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://mogul.capital/" />
<link rel="canonical" href="https://mogul.capital/" />
<meta property="og:title" content="Mogul Capital | Your Fastest Growing Wealth Management Partner" />
<title>Mogul Capital | Your Fastest Growing Wealth Management Partner</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<div class="width100 same-padding banner1 text-center">
	
    <p class="ow-first-p black-text first-p white-text wow fadeIn" data-wow-delay="0.3s">Your Fastest Growing Wealth Management Partner</p>
    <h1 class="darkgold-text first-h1 white-text wow fadeIn" data-wow-delay="0.6s">Mogul Capital</h1>
</div>
<div class="width100 same-padding banner2 overflow">
		<p class="black-text title-p wow fadeIn text-center ow-title-p" data-wow-delay="1s"><b>About</b> Us</p>
        <div class="short-gold-border text-center wow fadeIn" data-wow-delay="1.3s"></div>
    <div class="banner2-left-content">
    	<h1 class="darkgold-text banner2-h1 wow fadeIn" data-wow-delay="1.8s">Unique Blend of<br><b>Wealth Management</b></h1>
    </div>
	<div class="banner2-right-content">
    	
        <p class="content-p wow fadeIn ow-content-p black-text" data-wow-delay="2.1s">
        	Malaysia-based Mogul Capital is a locally established and SSM registered Capital company with a unique blend of Wealth Management offering opportunities aimed at the global market stemming from Malaysia.<br><br>

Built within with a team of <b>experienced entrepreneurial personnel, aggressive and disciplined investors</b>, Mogul Capital is well-equipped to spot and execute precisely esteemed <b>investment opportunities.</b><br><br>

The competent fund managers are self-made millionaires from various walks of life, ranging from a 20-year traditional business trader, investor and real estate investor to a serial entrepreneur and serial commercial project developer for 8 years.
        
        </p>
    </div>

</div>
<div class="width100 same-padding dark-bg overflow">
		
		<p class="darkgold-text title-p wow fadeIn ow-title-p" data-wow-delay="0.2s"><b>Fund Highlight:</b> ALIAC Sub-Fund 1</p>
        <div class="short-gold-border wow fadeIn ow-gold-border" data-wow-delay="0.5s"></div>
        <div class="ow-p-margin margin-top20">
            <p class="content-p white-text text-center wow fadeIn" data-wow-delay="0.8s">Current investment value: <b>RM2,570,000</b></p>
            <p class="content-p white-text text-center wow fadeIn" data-wow-delay="1.1s">ROI: <b>2.57x (257%) p.a.</b></p>
            <p class="content-p white-text text-center wow fadeIn" data-wow-delay="1.4s">Initial investment: <b>RM1,000,000</b></p>
            <p class="content-p white-text text-center wow fadeIn" data-wow-delay="1.7s">FUND CLOSED.</p>
            <p class="content-p white-text text-center wow fadeIn" data-wow-delay="2s">NEW FUND OPENING - <b>March 15th, 2021</b></p>
        </div>
        
        
        <!--<p class="btc-p text-center white-text wow fadeIn"  data-wow-delay="2.3s"><b>BTC/BUSD</b></p>-->


        <!-- <div id="chart" class="wow fadeIn"  data-wow-delay="2.6s"></div>
        <div id="chartBTC" class="wow fadeIn" data-wow-delay="2.6s"></div> -->
        <div id="chartBTC" class="wow fadeIn"></div>
        </div>
</div>
<div class="width100 same-padding banner3">
		<p class="white-text title-p wow fadeIn text-center ow-title-p" data-wow-delay="0.2s"><b>Invest</b>ment</p>
        <div class="short-gold-border text-center wow fadeIn" data-wow-delay="0.5s"></div>
        <div class="clear"></div>
        <div class="width103">
            <div class="white-card-css wow fadeIn top-white" data-wow-delay="0.8s">
            	<p class="content-p black-text">
                	Mogul Capital is actively investing funds into various Alternative Liquid Investment Asset Classes (ALIAC Main-Fund).
                </p>
            </div>
            <div class="white-card-css wow fadeIn top-white" data-wow-delay="1.1s">
            	<p class="content-p black-text">
                	Recognizing the investment market is filled with various opportunities and its potentials, Mogul Capital deploys its Proprietary Risk Assessment Modelling (MC-PRAM) to assess risk-reward benefits and also hedges the best leverage over the potential risks of the investment.
                </p>
            </div>
            <div class="white-card-css wow fadeIn bottom-white" data-wow-delay="1.4s">
            	<p class="content-p black-text">
                	We firmly believe that no man is an island when it comes to deal closing and risk management, where necessary, Mogul Capital deploys Quantitative Analysis synergizing with our Resident Data Science Investors to perform quantitative analysis on trends, market deals and macro-economic outlooks as befits the enhancement of opportunities and risk management.
                </p>
            </div>
            <div class="white-card-css wow fadeIn bottom-white" data-wow-delay="1.7s">
            	<p class="content-p black-text">
                	Within the ALIAC Main-Fund, the funds are then segregated into various sub-funds with its own fund managers governing the coded heuristics within the principles of Mogul Capital which is maximizing investors return leveraging qualified and quantified information while hedging on possible alternative options to allow investors a guaranteed return while engaging in an aggressive but stress-free investment environment.
                </p>
            </div>


  
		</div>
  </div>
  <div class="info-div2 padding-top same-padding">
  	<div class="three-div-css ">
    	<img src="img/ai2.png" class="three-icon wow fadeIn" data-wow-delay="0.2s" alt="AI Investment" title="AI Investment">
            <p class="three-title-p darkgold-text wow fadeIn" data-wow-delay="0.4s">ALIAC Sub-Fund 1</p>
            <p class="three-content-p gold-text wow fadeIn" data-wow-delay="0.6s">MCH-Codified Quantitative Driven AI Investment (Decision Making – Stocks/Crypto)</p>        
    </div>
  	<div class="three-div-css mid-three-css second-three-div">
    	<img src="img/bitcoin2.png" class="three-icon wow fadeIn" data-wow-delay="0.8s" alt="Crypto-currency" title="Crypto-currency">
            <p class="three-title-p darkgold-text wow fadeIn" data-wow-delay="1s">ALIAC Sub-Fund 2</p>
            <p class="three-content-p gold-text wow fadeIn" data-wow-delay="1.2s">Crypto-currency and its technology</p>        
    </div>
    <div class="tempo-two-clear"></div>
  	<div class="three-div-css">
    	<img src="img/product2.png" class="three-icon wow fadeIn" data-wow-delay="1.4s" alt="Products" title="Products">
            <p class="three-title-p darkgold-text wow fadeIn" data-wow-delay="1.6s">ALIAC Sub-Fund 3</p>
            <p class="three-content-p gold-text wow fadeIn" data-wow-delay="1.8s">Probabilistic Speculation on Non-Financial Products (CFD Pure)</p>        
    </div>
    <div class="tempo-three-clear"></div>
  	<div class="three-div-css second-three-div">
    	<img src="img/exchange2.png" class="three-icon wow fadeIn" data-wow-delay="2s" alt="Commodity Exchanges & FOREX" title="Commodity Exchanges & FOREX">
            <p class="three-title-p darkgold-text wow fadeIn" data-wow-delay="2.2s">ALIAC Sub-Fund 4</p>
            <p class="three-content-p gold-text wow fadeIn" data-wow-delay="2.4s">Quantitative Performance of Arbitrage in Exchanges (Commodity Exchanges & FOREX)</p>        
    </div>
    <div class="tempo-two-clear"></div>
  	<div class="three-div-css mid-three-css">
    	<img src="img/ipo2.png" class="three-icon wow fadeIn" data-wow-delay="2.6s" alt="pre-IPO Market Making" title="pre-IPO Market Making">
            <p class="three-title-p darkgold-text wow fadeIn" data-wow-delay="2.8s">ALIAC Sub-Fund 5</p>
            <p class="three-content-p gold-text wow fadeIn" data-wow-delay="3s">Speculative and pre-IPO Market Making</p>        
    </div>


    <div class="clear"></div>       

  </div>
</div>
<div class="width100 same-padding banner4 overflow">
	<div class="text-center width100 timeline-div">
		<img src="img/timeline.png" class="timeline wow fadeIn"  data-wow-delay="0.2s">
    </div>
	<div class="clear timeline-clear"></div>
    <div class="after-timeline">
        <div class="three-div-css three-div-p white-text wow fadeIn"  data-wow-delay="0.5s">
            Based on the above Sub-Funds, Mogul Capital is able to pro-actively capped on a guaranteed minimal 2% per month return (24% annualized) based the robust team and proven track-record over the past 6-month investment horizon of a minimal 8% monthly return.
        </div>
        <div class="three-div-css mid-three-css three-div-p white-text wow fadeIn"  data-wow-delay="0.8s">
            We are proud to announce our fund that has already surpassed our 2021 benchmark attributed to the soaring records of Bitcoin (ALIAC-SF1 and ALIAC-SF2) allowing our investments to be in multiple folds when we close our positions.
        </div>
        <div class="three-div-css three-div-p white-text wow fadeIn"  data-wow-delay="1.1s">
            As a secondary thematic focus, we are also focused in supporting and creating a future fund for pioneering startups that are in-line with our Mogul Capital’s principles and heuristics.
        </div>
    </div>    
</div>
<div class="width100 same-padding padding-top">

  
		<p class="black-text title-p wow fadeIn text-center ow-title-p" data-wow-delay="0.2s"><b>Contact</b> Us</p>
        <div class="short-gold-border text-center wow fadeIn" data-wow-delay="0.5s"></div>
        <div class="clear"></div>
        <div class="width75 overflow">
            <p class="content-p wow fadeIn black-text ow-content-p3" data-wow-delay="0.8s">

    
    For more investment information related to Mogul Capital, please call the following number indicating your interest in the specific investment opportunities to secure an appointment with our investment advisor/fund manager.
            
            </p>
                <div class="width100">
              
                        <img src="img/call.png" class="call-icon inline-block margin-right10 wow fadeIn" data-wow-delay="1.1s" alt="Call" title="Call">

                        <p class="ctitle-p gold-text call-text inline-block wow fadeIn" data-wow-delay="1.4s"><a href="tel:+60165324691" class="gold-text call-text">+60165324691</a></p>
   				</div>   
                <div class="width100 overflow call-div">
                    <div class="left-icon-div wow fadeIn call-left" data-wow-delay="1.7s">
                        <img src="img/address.png" class="left-icon call-icon margin-right10" alt="Address" title="Address">
                    </div>
                    <div class="right-content-div wow fadeIn" data-wow-delay="1.9s">
                        <p class="ctitle-p gold-text call-text">1-3-07, Summerskye Commercial Square, Jalan Sungai Tiram, 11950 Bayan Lepas, Pulau Pinang</p>
                        
                    </div> 
   				</div>  
        </div>


</div>  
<div class="width100 gmap-div">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3972.7687684224506!2d100.2656819144798!3d5.2987365961559725!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x304abffb10295c85%3A0x1c90f81946e8fde5!2sSummerSkye%20Residences!5e0!3m2!1sen!2smy!4v1614854439951!5m2!1sen!2smy" class="gmap wow fadeIn" data-wow-delay="2.3s" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
</div>


<?php include 'js.php'; ?>

<script type="text/javascript">
    $(document).ready(function()
    {
        $("#chartBTC").load("index4.php");
    setInterval(function()
    {
        $("#chartBTC").load("index4.php");
    }, 15000);
    });
</script>

</body>
</html>